import base64
import os
from cryptography.fernet import Fernet, InvalidToken
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import click

SALT_LENGTH = 64

@click.command()
@click.option('--password', prompt=True, hide_input=True)
def decrypt(password):
	secret = bytes(password, 'utf-8')
	with open('fernet.fnt', 'rb') as secret_file:
		salt = secret_file.read(SALT_LENGTH)
		kdf = PBKDF2HMAC(
		    algorithm=hashes.SHA256(),
		    length=32,
		    salt=salt,
		    iterations=100001,
		    backend=default_backend()
		)
		key = base64.urlsafe_b64encode(kdf.derive(secret))
		f = Fernet(key)
		message = secret_file.read()
		try:
			print(f.decrypt(message).decode('utf-8'))
		except InvalidToken:
			print('You\'ve entered a wrong password!')

if __name__ == '__main__':
	decrypt()