import base64
import os
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import click

SALT_LENGTH = 64

@click.command()
@click.argument('filename')
@click.password_option()
def encrypt(filename, password):
	secret = bytes(password, 'utf-8')
	salt = os.urandom(SALT_LENGTH)
	with open(filename + '.fnt', 'wb') as secret_file:
		secret_file.write(salt)
		kdf = PBKDF2HMAC(
			algorithm=hashes.SHA256(),
			length=32,
			salt=salt,
			iterations=100001,
			backend=default_backend()
			)
		key = base64.urlsafe_b64encode(kdf.derive(secret))
		f = Fernet(key)
		with open(filename, 'rb') as file:
			message = file.read()
			secret_message = f.encrypt(message)
			secret_file.write(secret_message)

if __name__ == '__main__':
	encrypt()